const URL: string = 'https://www.googleapis.com/books/v1/volumes';
const PARTIAL_REQUEST_URL: string = 'items(id,volumeInfo/description,volumeInfo/title,volumeInfo/authors,volumeInfo/imageLinks/thumbnail)';

const POTECTION_FACTOR_AGAINST_API: number = 2;
const ITEMS_PER_PAGE: number = 20;
const CURRENT_PAGE_NEIGHBOURING: number = 2;

export {
  URL,
  PARTIAL_REQUEST_URL,
  ITEMS_PER_PAGE,
  POTECTION_FACTOR_AGAINST_API,
  CURRENT_PAGE_NEIGHBOURING
};
