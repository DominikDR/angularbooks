import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Event, NavigationStart, NavigationEnd, RoutesRecognized, RouterEvent } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpParams } from '@angular/common/http';

import { tap, map, filter, subscribeOn, switchMap } from 'rxjs/operators';

import { BooksService } from '../books.service';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css'],
  providers: [BooksService],
})
export class BookListComponent implements OnInit {

  books
  isLoading: boolean = false;

  constructor(
    private booksService: BooksService,
    private http: HttpClient,
    private location: Location,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { };

  ngOnInit() {
    this.booksService.readPageFromRouter()
    this.setQueryParamsFromRoute();
    this.booksService.query && this.fetchBooksFromService()
  }

  setQueryParamsFromRoute(): void {
    const params = this.booksService.getParamsFromRoute();
    const isParams = Object.entries(params).length;
    if (isParams) {
      this.booksService.setQuery(params);
    }
  }

  updateList() {
    this.booksService.readPageFromRouter()
    this.booksService.setQuery(this.booksService.createObjectForQuery())
    this.fetchBooksFromService();
  }

  fetchBooksFromService() {
    this.isLoading = true;
    this.books = this.booksService.fetchBooks();
    this.books.pipe(
      tap(() => {
        this.isLoading = false;
      }),
      tap(() => {
        this.isLoading = false;
        console.error
      }
    )).subscribe()
  }
}
