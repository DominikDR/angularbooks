import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule }     from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BooksService } from './books.service';
import { BookListComponent } from './book-list/book-list.component';
import { LoaderComponent } from './loader/loader.component';
import { BookComponent } from './book/book.component';
import { PaginationComponent } from './pagination/pagination.component';
import { BookSearchComponent } from './book-search/book-search.component';
import { ValidateEmptyFieldsDirective } from './shared/validate-empty-fields.directive';

@NgModule({
  declarations: [
    AppComponent,
    BookListComponent,
    LoaderComponent,
    BookComponent,
    PaginationComponent,
    BookSearchComponent,
    ValidateEmptyFieldsDirective,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    NgbPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  providers: [ BooksService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
