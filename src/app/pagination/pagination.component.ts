import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { BooksService } from '../books.service';
import { CURRENT_PAGE_NEIGHBOURING } from '../../consts';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  @Output() handlePageChange: EventEmitter<any> = new EventEmitter<any>();

  pageStatus: any = {};
  isControlLeftArrow: boolean;
  isControlRightArrow: boolean;

  constructor(
    private booksService: BooksService,
  ) { }

  ngOnInit() {
    this.pageStatus = this.booksService.getPageStatus();
    const { totalPages, currentPage } = this.pageStatus
    this.pageStatus.visiblePages = this.getRangeOfVisiblePages(totalPages, currentPage);
  }

  onPageChange() {
    if (this.booksService.currentPage === this.pageStatus.currentPage) {
      return;
    }
    this.handlePageChange.emit()

    this.pageStatus = this.booksService.getPageStatus();
    const { totalPages, currentPage } = this.pageStatus;
    this.pageStatus.visiblePages = this.getRangeOfVisiblePages(totalPages, currentPage);
  }

  getRangeOfVisiblePages(totalPages, currentPage): number[] {
    const { createArray } = this.booksService;

    const currentPageNeighboursRange = CURRENT_PAGE_NEIGHBOURING * 2 + 1; // current page in the middle and both sides neighbours
    const isEnoughTotalPages = currentPageNeighboursRange < totalPages.length;
    if (!isEnoughTotalPages) {
      return totalPages;
    }

    const bound = currentPage - CURRENT_PAGE_NEIGHBOURING;
    this.isControlLeftArrow = bound > CURRENT_PAGE_NEIGHBOURING;
    this.isControlRightArrow = currentPage + CURRENT_PAGE_NEIGHBOURING < totalPages.length - CURRENT_PAGE_NEIGHBOURING;
    const isControlLeftArrow = this.isControlLeftArrow;
    const isControlRightArrow = this.isControlRightArrow;

    const offsetDueToArrow = 1;
    if (!isControlLeftArrow && isControlRightArrow) {
      const firstPage = this.pageStatus.totalPages[0];
      return createArray(currentPageNeighboursRange + offsetDueToArrow, firstPage + 1);
    }
    if (isControlLeftArrow && isControlRightArrow) {
      return createArray(currentPageNeighboursRange, bound);
    }
    if (isControlLeftArrow && !isControlRightArrow) {
      const rightBound = isControlRightArrow ? bound : totalPages.length - 1 - currentPageNeighboursRange;
      return createArray(currentPageNeighboursRange + offsetDueToArrow, rightBound)
    }
  }
}
