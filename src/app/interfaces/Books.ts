export interface Books {
  id: String;
  etag: String;
  title: String;
  authors: String[];
  cover: String;
  bookDescription: String;
}

export interface BookSearch {
  title: String;
  author: String;
  language: String;
}