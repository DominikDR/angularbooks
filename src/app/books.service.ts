import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, RoutesRecognized, RouterEvent } from '@angular/router';
import { Location } from '@angular/common';

import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, shareReplay, filter, switchMap } from 'rxjs/operators';

import { URL, ITEMS_PER_PAGE, POTECTION_FACTOR_AGAINST_API } from './../consts';
import { Books, BookSearch } from './interfaces/Books';


@Injectable({
  providedIn: 'root',
})
export class BooksService {
  books

  isLoading: boolean = false;
  totalItemsNumber: number;
  currentPage: number = 1;
  page
  bookSearchFormData: BookSearch = {
    title: '',
    author: '',
    language: '',
  };
  navigationEnd: Observable<NavigationEnd>;
  query;

  constructor(
    private http: HttpClient,
    private location: Location,
    public router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
  }

  setSearchedData(searchedData) {
    this.bookSearchFormData = searchedData;
  }

  parseObjectForQuery (parametersObject, wordSeparator, parametersSeparator) {
    return Object.entries(parametersObject)
      .filter((value) => value[1] !== undefined && value[1] !== '')
      .map((value) => value.join(wordSeparator))
      .join(parametersSeparator);
  };

  createObjectForQuery() {
    const startIndex = this.getStartIndex(this.currentPage);
    const { language, ...titleAndAuthor } = this.bookSearchFormData;
    return {
      q: this.parseObjectForQuery(titleAndAuthor, ':', '+'),
      language,
      maxResults: ITEMS_PER_PAGE,
      startIndex,
    };
  }

  createQuery(objectForQuery): string {
    const query = this.parseObjectForQuery(objectForQuery, '=', '&')
    if (objectForQuery.q) {
      return `?${query}`;
    }
    return `?q=''&${query}`
  }

  setQuery(objectForQuery) {
    this.query = this.createQuery(objectForQuery);
  }

  getParamsFromRoute() {
    let queryObject;
    this.activatedRoute.queryParams.subscribe(params => {
      queryObject = params;
    });
    console.log("TCL: BooksService -> getParamsFromRoute -> queryObject", queryObject)
    return queryObject;
  }

  readPageFromRouter() {

    //https://medium.com/@tomastrajan/how-to-get-route-path-parameters-in-non-routed-angular-components-32fc90d9cb52
    /* this.navigationEnd = this.router.events.pipe(
      filter((event: RouterEvent): event is NavigationEnd => event instanceof NavigationEnd)
      );
    console.log("TCL: BookListComponent -> ngOnInit -> this.navigationEnd", this.navigationEnd)

    this.page = this.navigationEnd
      .pipe(
        map(() => this.activatedRoute.root.firstChild),
        switchMap(firstChild => {
          if(firstChild) {
            console.log("TCL: BookListComponent -> readPageFromRouter -> firstChild", firstChild)
            const targetRoute = firstChild.firstChild;
            console.log("TCL: BookListComponent -> readPageFromRouter -> targetRoute", targetRoute)
            return targetRoute.paramMap.pipe(map(paramMap => paramMap.get('page')))
          } else {
            return of(null)
          }
        })
      ); */

    this.router.events.subscribe((data) => {
      if (data instanceof RoutesRecognized && data.state.root.firstChild) {
        map(() => {
          const page = parseInt(data.state.root.firstChild.params.page);
          console.log("this.page", this.page)
          this.setPage(page)
        })
      }
    });
  }

  fetchBooks(): Observable<Books[]> {
    this.location.go(`/books/${this.currentPage}${this.query}`);
    //this.location.go(this.query);
    return this.http.get(`${URL}${this.query}`)
      .pipe(
        map((data: any) => {
        this.totalItemsNumber = data.totalItems;
        return this.extractedDataBooks(data)
      }),
      shareReplay());
  }

  extractedDataBooks(data) {
    return data.items.map(book => {
      const { id, etag, volumeInfo: { title, authors, description, imageLinks } } = book;
      const cover = imageLinks ? imageLinks.thumbnail : null
      const bookDescription = description || 'This book has no description';
      return {
        id,
        etag,
        title,
        authors,
        cover,
        bookDescription,
      }
    });
  }

  getStartIndex(page) {
    return page * ITEMS_PER_PAGE - ITEMS_PER_PAGE;
  }

  createArray(arrayLength: number, arrayStartNumber: number): number[] {
    return Array.from(Array(arrayLength), ((e, i) => i + arrayStartNumber));
  }

  getPageStatus() {
    let totalPages: number[];
    let lastPage: number;
    if (this.totalItemsNumber) {
      lastPage = Math.ceil(this.totalItemsNumber / (ITEMS_PER_PAGE * POTECTION_FACTOR_AGAINST_API))
      totalPages = this.createArray(lastPage, 1);
    }
    return {
      currentPage: this.currentPage,
      totalItems: this.totalItemsNumber / 2,
      lastPage,
      totalPages,
    };
  }

  setPage(page) {
    this.currentPage = page;
  }
}
