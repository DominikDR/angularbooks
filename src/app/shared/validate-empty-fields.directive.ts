import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, ValidationErrors } from '@angular/forms';

const checkEmptyField = (fieldName, control: AbstractControl) => {
  const fieldValue = control.get(fieldName).value;
  return fieldValue && fieldValue.length > 0 ? true : false;
}

export const validateEmptyFields: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  return ['intitle', 'inauthor'].some(fieldName => checkEmptyField(fieldName, control))
    ? null
    : ({ isFieldEmpty: true } as ValidationErrors);
}

@Directive({
  selector: '[appValidateEmptyFields]',
  providers: [{ provide: NG_VALIDATORS, useExisting: ValidateEmptyFieldsDirective, multi: true }]
})
export class ValidateEmptyFieldsDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors {
    return validateEmptyFields(control)
  }
}
