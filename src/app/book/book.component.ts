import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  @Input() id: string;
  @Input() etag: string;
  @Input() cover: string;
  @Input() title: string;
  @Input() authors: string[];


  constructor() { }

  ngOnInit() {
  }

}
