import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { validateEmptyFields } from '../shared/validate-empty-fields.directive';

import { BooksService } from '../books.service';

export const formInputName = {
  author: 'inauthor',
  title: 'intitle',
  language: 'bookLanguage',
}

@Component({
  selector: 'app-book-search',
  templateUrl: './book-search.component.html',
  styleUrls: ['./book-search.component.css']
})
export class BookSearchComponent implements OnInit {
  searchForm: FormGroup;
  submitted: boolean = false;

  @Output() handleSearch: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private booksService: BooksService,
    private formBuilder: FormBuilder,
  ) { };

  submitMessage: string = 'Enter proper data';

  get control() {
    return this.searchForm.controls;
  }
  resetSubmitted() {
    this.submitted = false;
  }

  onSubmit() {
    this.submitted = true;
    this.booksService.setSearchedData(this.searchForm.value);
    //this.handleSearch.emit();
  }

  ngOnInit() {
    this.searchForm = new FormGroup ({
      [formInputName.author]: new FormControl(''),
      [formInputName.title]: new FormControl(''),
      [formInputName.language]: new FormControl(''),
    }, { validators: validateEmptyFields})
  }
}
